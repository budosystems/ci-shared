#!/usr/bin/env bash

# Ensure the variable is defined
${PY_VERSIONS:?}

source $HOME/.profile

for v in ${PY_VERSIONS}; do
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    echo \$v = $v
    echo Calling \"pyenv install $v\"
    pyenv install $v
    echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
done

pyenv global $PY_VERSIONS

UPGRADE="-m pip install -U pip setuptools nox"
for v in ${PY_VERSIONS}; do
    echo '**************************************************************************'
    pv=${v%.0rc*}
    pv=${pv%-dev}
    echo Calling \"python${pv} ${UPGRADE}\"
    python${pv} ${UPGRADE}
    echo '**************************************************************************'
done

# test directories are over 100MB each, safe to delete them to save some space on the final image.
cd .pyenv/versions
pwd
for v in *; do
    echo '--------------------------------------------------------------------------'
    echo $v
    echo $v/lib/python3.*/test
    rm -rf $v/lib/python3.*/test
    echo $v/lib/python3.*/config-*
    rm -rf $v/lib/python3.*/config-*/
    echo '--------------------------------------------------------------------------'
done
