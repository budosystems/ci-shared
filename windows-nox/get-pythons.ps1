
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

Install-Package ThreadJob -Force
Import-Module ThreadJob


Set-PSDebug -Strict -Trace 0
#$DebugPreference = "Continue"


$PYS = ${env:PY_VERSIONS}.Split(' ')
$ARCH = (Get-CimInstance Win32_operatingsystem).OSArchitecture
if ($ARCH -like "*64*") {
    $PY_ARCH = "-amd64"
}
else {
    $PY_ARCH = ""
}
Write-Host "ARCH='$ARCH', PY_ARCH='$PY_ARCH'"

$jobs = @()

foreach ($py in $PYS) {
    $jobs += Start-ThreadJob -Name "Download Python $py Installer" -StreamingHost $Host -ScriptBlock {
        param($py)
        Write-Debug "py=${py}"

        $pyExe = "python-${py}${using:PY_ARCH}.exe"
        Write-Debug "pyExe=${pyExe}"

        $pyPattern = "\d+\.\d+\.\d+"
        $py -match $pyPattern
        $pyDir = $Matches[0]
        Write-Debug "pyDir=${pyDir}"


        Write-Host "Starting to download ${pyExe}"
        $webRequestArgs = @{
            UseBasicParsing = $true
            Uri = "https://www.python.org/ftp/python/${pyDir}/${pyExe}"
            OutFile = "${pyExe}"
            PassThru = $true
        }

        try {
            $Response = Invoke-WebRequest @webRequestArgs
            $StatusCode = $Response.StatusCode
            Write-Host "${pyExe} finished downloading, Status Code: ${StatusCode}"
        }
        catch {
            $Response = $_.Exception.Response
            $StatusCode = $Response.StatusCode.value__
            throw "${pyExe} not downloaded, Status Code: ${StatusCode}"
        }
    } -ArgumentList $py
}


$JobResults = Wait-Job -Job $jobs
foreach ($job in $JobResults) {
    Receive-Job $job
}
