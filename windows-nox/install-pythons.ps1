Set-PSDebug -Strict -Trace 0

function Install-Python($PyRoot, $py, $ARCH) {
    if ($ARCH -like "*64*") { $PY_ARCH =  "-amd64" } else { $PY_ARCH = "" }
    Write-Host "ARCH='$ARCH', PY_ARCH='$PY_ARCH', PyRoot='$PyRoot'"
    $PyPath = (Join-Path $PyRoot $py)
    Write-Host "Starting installation of Python $py ($ARCH) in ${PyPath}"
    $PyExe = "python-${py}${PY_ARCH}.exe"
#    Get-ChildItem .
    if (-not (Test-Path $PyExe)) {
        throw "$PyExe not found."
    }
    try {
        $installArgs = @(
            "/quiet"
            "/log"
            "python-${py}-install.log"
            "InstallAllUsers=1"
            "TargetDir=${PyPath}"
            "PrependPath=1"
            "Include_doc=0"
            "Include_test=0"
            "Include_tcltk=0"
            "Shortcuts=0"
        )
#        Write-Host $installArgs
        Start-Process $PyExe -Wait -ArgumentList $installArgs

        # Refresh the PATH environment variable
        $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")

        # Get-Content "python-${py}-install.log"
        Get-ChildItem $PyPath -ErrorAction Stop
        Write-Host "Completed installation of Python $py ($ARCH) in ${PyPath}"
        Write-Host $env:Path
    }
    catch {
        Write-Error "Failed installation of Python $py ($ARCH) in ${PyPath}"
        Write-Error $_
        throw
    }
}

function Get-PythonVersionVariants($py) {
    $pyVersions = @()
    $pySplit = $py.Split(".")
    foreach ($p in 0..($pySplit.Length - 1)) {
        $pyVersions += ($pySplit[0..$p] -join ".")
    }
    return $pyVersions
}

function Install-PythonSymlinks($pyRoot, $py) {
    Write-Host "Installing SymLinks for Python $py"
    $pyVersions = Get-PythonVersionVariants $py
    Write-Host "Versions [$pyVersions]"
    $pyPath = $(Join-Path $pyRoot $py)
    foreach ($ver in $pyVersions) {
        $link = $(Join-Path $pyPath "python${ver}.exe")
        $target = $(Join-Path $pyPath "python.exe")
        New-Item -itemtype SymbolicLink -Path $link -Target $target
    }
    Write-Host "Finished Installing SymLinks for Python $py"
}

function Install-Nox($pyRoot, $py) {
    $pipInstallCmd = "-m", "pip", "install", "-U"
    $pipPackages = "pip", "setuptools", "nox"
    $pyPath = $(Join-Path $pyRoot $py)
    $pyExe = $(Join-Path $pyPath "python.exe")

    try {
        Write-Host "Installing packages [$pipPackages] on Python $py"
        $args = $pipInstallCmd + $pipPackages
        Write-Host Running "Start-Process $pyExe -Wait -NoNewWindow -ArgumentList $args"
        Start-Process $pyExe -Wait -NoNewWindow -ArgumentList $args
        Write-Host "Package install completed on Python $py"
    }
    catch {
        Write-Host "Failed package installation for Python $py"
        Write-Host $_
    }
}

function Install-Pythons($PyRoot, $PYTHONS, $ARCH) {
    foreach ($py in $PYTHONS) {
        Install-Python $PyRoot $py $ARCH
        Install-PythonSymlinks $PyRoot $py
        Write-Host $env:Path
        Install-Nox $PyRoot $py
    }
}

function main() {
    $PYS = ${env:PY_VERSIONS}.Split(' ')
    $ARCH = (Get-CimInstance Win32_operatingsystem).OSArchitecture
    $PyRoot = "C:\Python"

    mkdir $PyRoot

    Install-Pythons $PyRoot $PYS $ARCH
}

main
