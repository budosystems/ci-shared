@echo off
setlocal enabledelayedexpansion
cd C:\Python\
reg add HKCU\Environment
for %%I in (%PY_VERSIONS%) do set PY=%%~fI\Scripts;%%~fI;!PY!
set PATH=%PY%;%PATH%
setx PATH %PATH%
