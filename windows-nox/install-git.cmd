@ECHO OFF
SET GIT_VER=2.45.2
SET GIT_MICRO=1
IF %GIT_MICRO% EQU 1 (
    SET GIT_ZIP="MinGit-%GIT_VER%-64-bit.zip"
) ELSE (
   SET GIT_ZIP="MinGit-%GIT_VER%.%GIT_MICRO%-64-bit.zip"
)
SET GIT_URL="https://github.com/git-for-windows/git/releases/download/v%GIT_VER%.windows.%GIT_MICRO%/%GIT_ZIP%"

SET GIT_ROOT=C:\Git
SET RETRY=5

curl -L %GIT_URL% -o %GIT_ZIP% --retry %RETRY% --retry-delay 10
IF %ERRORLEVEL% NEQ 0 (
   echo "Download failed after retrying %RETRY% times." 1>&2
   exit /b %ERRORLEVEL%
)

md %GIT_ROOT%
tar -xf %GIT_ZIP% -C %GIT_ROOT%
SET GIT_PATH=%GIT_ROOT%\cmd
SET PATH=%GIT_PATH%;%PATH%
SETX PATH "%PATH%"
